# FSO Student Interview problem

A quick post-interview problem for prospective student employees. This is intented to only take a few hours and demonstrate knowledge of using a coding language and it's native (or 3rd-party) libraries.

![kerbal gif](https://media.giphy.com/media/5SVCRPaD3GBqw/giphy.gif)

## The Task at Hand

You can use any language for this task, and we recommend you use what you are most comfortable with and which demonstrates your best programming skills. While we (the interviewers) may have no experience with a language, part of this task is to include instructions for running your code. The world is your oyster. At FSO we mostly use Python, Javascript, and C#, but many of us also have experience (or at least familiarity) in C, Java, Ruby, and other popular languages.

Please read all the steps before proceeding.

### Steps
1. **Read the attached csv file (`kerbals.csv`).** This is a fairly standard csv with quoted text values and a standard delimiter (`,`). Feel free to utilize any built-in csv reading libraries (most languages have one) or utilize a 3rd-party library\*.
2. **Output (print) the data as JSON.** Your program, when run, should read `kerbals.csv` and output a `kerbals.json` file _and_ print the json to the console. Each object in the JSON array should correspond to one row from the `csv`, and the keys of the JSON should correspond to the column header. Again, feel free to utilize any built-in JSON libraries (again, most languages have one) or utilize a 3rd-party library\*.
    - We will be checking your output visually and with a linter. Specifically, [jsonlint](https://github.com/zaach/jsonlint). That means we should be able to read your generated `kerbals.json` file with that library `jsonlint kerbals.json` _and_ pipe the output of your program to it `./yourprogram | jsonlint`. We recommend you check yourself by doing the same! If you don't want to install the library on your machine you can use [https://jsonlint.com/]().
3. **Write a detailed README.md** Include a detailed readme using standard markdown syntax (feel free to add Gitlab/Github flavored features if you want). This file should include a description of your program as well as instructions for running it. You can assume we will run it on a unix/unix-like system (we use Macs), but if you are using a windows machine you can provide us instructions for windows environment\*\*.
    - For instance, if you are using python, the instructions will likely include running `python main.py` (or whatever you name your python file).
    - If your language requires several commands (i.e. more than 3 or 4) feel free to provide a shell script that can be used to run your program.
    - If your language/framework is not one of the following, please provide installation instructions for the language as well: Python, Node.js, C/C++, Ruby, Java, C# or any .NET language. If you write in Java and make use of Maven or Gradle, please note which one you use.
4. **Upload your program to a git repository.** The final step is to upload your program to any Git repository. Github, Gitlab, Bitbucket, or any other publicly accessible git service is acceptable. We should be able to clone your project and get going with it. You should include:
    - your program
    - `kerbals.csv`
    - `README.md`
    - any supporting files

**DO NOT** include your outputted `kerbals.json` file in the repository. We should generate it fresh after running your program. Bonus points if you utilize a `.gitignore` file to keep it from being uploaded to the repository.

\* If you choose to use 3rd-party packages, you _must_ install them locally within your codebase _or_ use a package manager. For example, if you write your sample in javascript and use [npm](https://npmjs.org), you'll need to include a `package.json` file in your codebase and instructions for using `npm`.

\*\* Be sure to note that you wrote the program on a windows machine, otherwise we might assume problems running the program are from you, not the environment you were working with.

### Bonus Points
You'll receive some extra credit for doing any of the following, but it is not required. We want quality, so if you provide us something extra that is buggy/doesn't work, the overall look of your submission will be tarnished.
- Use of a package manager and at least one third party library.
- Wrapping your program as a cli tool, granting the ability to pass an option to your program to output to the console _or_ to a file. (Be sure to note this in your usage instructions or you'll lose points) Doing this may require some sort of cli-integration library depending on your language. For example, we could run `python main.py --file` to output to a file, or something similar.
- Ability to export to another data format, such as XML, yaml, toml, etc. This can be done as a seperate program or via cli flags like above.
- Include a language-native linting check in your program that lints the JSON output and prints the validity to the console.
- Something else equally interesting or exciting!

## Time to complete
Please do not spend more than an a few hours on the required steps. If it requires more time, that's fine. Feel free to take as much time as you want on any bonus steps.

Again, a reminder: Bonus points can reflect badly on you if they are badly implemented! 

## Wait, I've never used git before!
Git is extremely easy! You can learn the basics required for this project in under an hour and there are a nearly unlimited number of resources for learning git available for free on the internet. How you use git doesn't matter to us: we just need to be able to clone your library.

## Kerbals?
Kerbals are a fictional alien species from the game Kerbal Space Program.
